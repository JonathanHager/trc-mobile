/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dungeoncrawler.model.entities;

import com.dungeoncrawler.StaticMath;
import com.dungeoncrawler.model.Entity;
import com.dungeoncrawler.model.Inventory;
import com.dungeoncrawler.model.Item;
/**
 *
 * @author Jan
 */
public class Player extends Entity {
    
    float standartDmg;
    float standartMaxHp;
    
    int skin;
    String gender;
    float standartMovementSpeed;
    float movementSpeed;

    int level;
    
    public Player() {
        super(200, 200, 1);
        this.skin = 0;
        this.lvl = 1;
        this.gender = "m";
        this.maxhp = 50 * lvl;
        this.hp = this.maxhp;
        this.standartMaxHp = 50 * lvl;
        this.standartDef = 2 * lvl;
        this.def = standartDef;
        
        this.dmg = 20*lvl;
        this.standartDmg = this.dmg;
        id = -1;
        type = -1;
        inv = new Inventory();
        // TODO: Sinnvolle Werte finden
        this.targetsPlayer = false;
        
        this.standartMovementSpeed = 3;
        this.movementSpeed = this.standartMovementSpeed;
        
    }
    
    public void updateStats(int ey){
        this.lvl = ey+1;
        this.level = (this.exp / 10) + 1;
        this.standartMaxHp = 50 * this.level;
        this.standartDmg = 20 * this.level;
        this.standartDef = 4 * this.level;
        this.def = this.standartDef;
        updateItems();
    }
    
    public void pickUp(Item item){
        inv.addItem(item);
    }
    public Inventory getInv(){
        return inv;
    }

    public void updateItems(){
        this.dmg = this.standartDmg;
        this.maxhp = this.standartMaxHp;
        this.def = this.standartDef;
        this.movementSpeed = this.standartMovementSpeed;

        if(inv.getItem(0) != null){
            this.dmg += inv.getItem(0).getDmg();
            this.maxhp += inv.getItem(0).getExtraHp();
            this.def += inv.getItem(0).getDef();
            this.movementSpeed += inv.getItem(0).getMovementBoost();
        }
        if(inv.getItem(1) != null){
            this.dmg += inv.getItem(1).getDmg();
            this.maxhp += inv.getItem(1).getExtraHp();
            this.def += inv.getItem(1).getDef();
            this.movementSpeed += inv.getItem(1).getMovementBoost();
        }
        if(hp > maxhp){
            hp = maxhp;
        }
        //System.out.println("New MaxHp: " + maxhp);
            
    }
    
    public void useItem(int x){
        if(inv.getItem(x) != null){
            updateItems();
            switch(inv.getItem(x).getId()){
                case 0:
                    // nix lol weil key
                    break;
                case 1:
                    if(this.hp == this.maxhp) break;
                    if(this.hp + inv.getItem(x).getHeal() >= this.maxhp){
                        this.hp = this.maxhp;
                    }
                    else{
                        this.hp = hp + inv.getItem(x).getHeal();
                    }
                    inv.setItem(x, null);
                    //this.hp = this.maxhp;
                    break;
                case 2:
                    // nix lol weil amulet
                    break;
            }
            
        }
    }

    public boolean move(int x, int y){
        return false;
    }
    
    @Override
    public Entity shoot(int xPosPlayer, int yPosPlayer){
        Projectile a = null;
        
        if(!isToDelete()){
            double alpha = StaticMath.calculateAngle((int) this.xPos, (int) this.yPos, xPosPlayer, yPosPlayer);
            
            a = new Projectile(this.xPos + 8, this.yPos + 40, this.lvl,(int) this.dmg,  5, false);
            int tempMovementX = (int) (12 * Math.cos(alpha));
            int tempMovementY = (int) (12 * Math.sin(alpha));

            a.setMovementX(tempMovementX + (0.7f * movementX));
            a.setMovementY(tempMovementY + (0.7f * movementY));
            a.setAngle(alpha);

            if((alpha >= 0 && alpha <= Math.PI / 2) || (alpha <= 2 * Math.PI && alpha >= 2 * Math.PI - Math.PI / 2)){
                setDirection(1);
            }
            else{
                setDirection(0);
            }
        }
        
        return a;
    }
    
    public boolean checkKey(){
        return inv.checkKey();
    }
    
    public void deleteKey(){
        inv.deleteKey();
    }
    
    public void setSkin(int i){
        skin = i;
    }
    public int getSkin(){
        return skin;
    }
    public void setGender(String i){
        gender = i;
    }
    public String getGender(){
        return gender;
    }
    public boolean inventoryFull(){
        return inv.inventoryFull();
    }
    public float getMovementSpeed(){
        return movementSpeed;
    }
    
    
}
